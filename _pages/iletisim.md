---
permalink: "/iletisim/"
layout: page
title: İletişim
---

<form action="https://formspree.io/{{site.email}}" method="POST">
    
    <div class="form-group row">

        <div class="col-md-6">
            <input class="form-control" type="text" name="name" placeholder="Ad Soyad">
        </div>

        <div class="col-md-6">
            <input class="form-control" type="email" name="_replyto" placeholder="E-Posta Adresi">
        </div>

    </div>

    <textarea rows="8" class="form-control mb-3" name="message" placeholder="Mesajınız"></textarea>
    
    <input class="btn btn-success" type="submit" value="Gönder">

</form>

<div>
    <br>
    E-posta: <a href="mailto:mehmet.kacar@softech.gen.tr"> mehmet.kacar@softech.gen.tr</a>
    <br>
    Telefon: <a href="tel:05337424205"> 0533 742 42 05</a>
</div>