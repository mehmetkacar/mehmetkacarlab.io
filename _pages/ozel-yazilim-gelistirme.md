---
permalink: "/ozel-yazilim-gelistirme/"
layout: page
title: Özel Yazılım Geliştirme
comments: false
---

Firmamız 1998 Yılında kurulmuştur. Global dünyada artan rekabetle beraber kendilerini yenilemek ve dijitalleşmek zorunda bulan müşterilerimize dijitalleşme ve otomasyon çözümleri üretiyoruz. Sürekli kendimizi yenileyerek müşterilerimize kaliteli hizmet verme anlayışını benimsemiş olup bilgi birikimimizle sektörde öncü olmayı hedeflemekteyiz.

Mehmet Kaçar.
